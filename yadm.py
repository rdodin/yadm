import os
import requests
from sys import exit
import yaml
import logging.config
from pprint import pprint, pformat

# CONSTANTS
DNS_DB_FNAME = 'dnsdb.yml'
headers = {'PddToken': 'yourYandexAPIToken'}  # a token to auth api calls
DOMAIN = 'yourdomain.net'  # your domain managed by Yandex DNS
add_url = 'https://pddimp.yandex.ru/api2/admin/dns/add'
get_url = 'https://pddimp.yandex.ru/api2/admin/dns/list?domain=nuageteam.net'
del_url = 'https://pddimp.yandex.ru/api2/admin/dns/del'
proxies = {
    'http': 'http://138.203.144.49:1080',
    'https': 'http://138.203.144.49:1080',
}


def setup_logging(
    default_path='log_setup.yml',
    default_level=logging.INFO
):
    """
    Setup logging configuration
    """
    path = default_path
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = yaml.safe_load(f.read())
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)


class termcolors:
    """
    class for coloring terminal output
    """
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def addDNSEntry(ip, subdomain):
            entry_params = {
                "domain": DOMAIN,
                "type": "A",
                "subdomain": subdomain,
                "content": ip
            }
            # if
            r = requests.post(add_url, params=entry_params, headers=headers, proxies=proxies)
            response = r.json()
            if response['success'] == 'ok':
                logger.info(termcolors.OKGREEN + '+' + termcolors.ENDC + ' DNS record of type A has been succesfully created: {} -> {}'.format(ip, response['record']['fqdn']))


def deleteDNSEntry(ip, subdomain, record_id):
    entry_params = {
        "domain": DOMAIN,
        "record_id": record_id
    }
    r = requests.post(del_url, params=entry_params, headers=headers, proxies=proxies)
    response = r.json()
    if response['success'] == 'ok':
        logger.info(termcolors.FAIL + '-' + termcolors.ENDC + ' DNS record of type A has been succesfully deleted: {} -> {}'.format(ip, subdomain))


def findDuplicateEntries(dns_db):
    '''
    Script does not allow multiple IPs for a single subdomain name
    This func looking duplicate entries and aborts script if one found
    '''
    dups_found = False
    rev_dns_db = {}
    for ip, dns_data in dns_db.items():
        for subdomain in [
                entry
                for entry in dns_data['subdomains']
                if entry is not None
        ]:
            rev_dns_db.setdefault(subdomain, set()).add(ip)

    for key, value in [(k, v) for k, v in rev_dns_db.items() if len(v) > 1]:
        logger.info(termcolors.FAIL + '! DUPLICATE FOUND:' + termcolors.ENDC + ' subdomain {} configured for a set of IP addresses: {}'.format(key, value))
        if dups_found is not True:
            dups_found = True
    if dups_found:
        logger.error("""\
        Please remove duplicates.
        Exiting...""")
        exit()


def sanitizeDB(dns_db):
    '''
    removes empty fields in subdomains section in YAML database
    empty fields are represented as None values during YAML import
    '''
    for ip, dns_data in dns_db.items():
        dns_data['subdomains'] = set([x for x in dns_data['subdomains'] if x is not None])


def parseRemoteDB(rem_dns_db):
    '''
    parse remote DNS database and format it
    in the same way as local DB is presented

    Returns:
    dict = {ip: {'subdomains': {subdomain_name: record_id}}}
    i.e.
    {'1.2.3.4': {'subdomains': {'test': 38772879}},
    '3.4.5.6': {'subdomains': {'nms.mts': 38327510, 'vsd.mts': 38318995}}
    }
    '''
    entries = {}
    for record in rem_dns_db['records']:
        if record['type'] == 'A':   # process only A records
            entries.setdefault(record['content'], dict()).setdefault('subdomains', dict())[record['subdomain']] = record['record_id']
    return entries


def compareDBs(localDNSDB, remoteDNSDB):
    '''
    comares local and remote databases
    and provides with a list of dicts
    with records to either add to remote DNS DB
    or delete from it

    localDNSDB example:
    {'217.195.72.1': {'comment': '', 'subdomains': {'test', 'test2'}},
     '217.195.72.2': {'comment': '', 'subdomains': {'test3'}},
     '217.195.72.3': {'comment': '', 'subdomains': set()}}

    remoteDNSDB example:
    {'1.2.3.4': {'subdomains': {'te-st': 38841414}},
     '217.195.72.1': {'subdomains': {'test': 38772879, 'test44': 38833269}},
     '217.195.72.3': {'subdomains': {'124': 38841535,
                                     'q': 38841533,
                                     'test123': 38841536}}
     }
    '''
    entriesToAdd = {}
    entriesToDelete = {}
    # TODO: check if remote data for an IP exists, use IF clauses accordingly
    for k, v in localDNSDB.items():
        if remoteDNSDB.setdefault(k):   # if remote DNS DB has IP as in local DB
            if v['subdomains'].difference(set(remoteDNSDB[k]['subdomains'])):
                entriesToAdd[k] = v['subdomains'].difference(set(remoteDNSDB[k]['subdomains']))

            # get entries that present in remote DNS DB and missing in local DB
            unwantedRemoteEntries = set(remoteDNSDB[k]['subdomains']).difference(v['subdomains'])
            if unwantedRemoteEntries:
                for e in unwantedRemoteEntries:     # populate a dict with record_ids of entries that need to be deleted
                    entriesToDelete.setdefault(k, dict()).update({e: remoteDNSDB[k]['subdomains'][e]})
        elif v['subdomains']:   # if remote DB has no matching IP, then all entries from local DB should be added if its not empty
            entriesToAdd[k] = v['subdomains']

    print("\n\n")
    logger.info('Entries To Add to RemoteDB: \n' + pformat(entriesToAdd))
    print("\n\n")
    logger.info('Entries To Delete from RemoteDB: \n' + pformat(entriesToDelete))

    return (entriesToAdd, entriesToDelete)


def main():
    setup_logging()
    global logger
    logger = logging.getLogger()
    try:
        with open(DNS_DB_FNAME, mode='r') as f:
            localDNSDB = yaml.load(f)   # load DNS data from local YAML file
            sanitizeDB(localDNSDB)      # remove empty and repetitive data
            findDuplicateEntries(localDNSDB)    # look for duplicates in local DNS DB
            logger.info(termcolors.OKBLUE + '#' + termcolors.ENDC + ' Fetching DNS entries for domain ' + termcolors.BOLD + '{}'.format(DOMAIN) + termcolors.ENDC + ' ...')
            # get remote DNS entries and parse contents
            remoteDNSDB = parseRemoteDB(requests.get(get_url, headers=headers, proxies=proxies).json())

            logger.info('Remote DB: \n' + pformat((remoteDNSDB)))
            print("\n\n")
            logger.info('Local DB: \n' + pformat(localDNSDB))
            # compare entries in remote and local DBs and get data to delete/add
            workingSet = compareDBs(localDNSDB, remoteDNSDB)
            if workingSet[0] or workingSet[1]:      # proceed if there is entries to add or delete
                logger.info(termcolors.BOLD + "\n\n======== ADDING/DELETING ENTRIES ========" + termcolors.ENDC)
                for ip, subdomains in workingSet[0].items():
                    for subdomain in subdomains:
                        addDNSEntry(ip, subdomain)

                for ip, subdomains in workingSet[1].items():
                    for subdomain, record_id in subdomains.items():
                        deleteDNSEntry(ip, subdomain, record_id)
            else:
                logger.info(termcolors.BOLD + "\n\n======== NO RECORDS TO ADD OR DELETE ========" + termcolors.ENDC)
    except OSError as err:
        logger.error("OS error: {0}".format(err))
    pass


if __name__ == "__main__":
    main()
