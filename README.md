# Yandex DNS manager (yadm)
Yadm script allows to manage DNS entries hosted on [Yandex DNS](https://domain.yandex.com) servers service via its [API](https://tech.yandex.com/domain/doc/concepts/api-dns-docpage/).
Refer to [this](https://yandex.com/support/domain/domain/dns.xml) document on how to delegate your domain to Yandex DNS.
![yadm demo](https://img-fotki.yandex.ru/get/98050/21639405.11d/0_8b621_d03d7d59_orig.gif)
## How to use
yadm will get DNS entries info from a local DNS database file and add/delete necessary entries to/from remote DNS database hosted by Yandex.

`python yadm.py` will run the script and provide with necessary outputs stramed to console and log file.
### DNS database
A local DNS database is maintained in a YAML file. A name of a file that is used by a script can be configured in the `CONSTANTS` section in the very beginning of a script.
By default the script looks for a file with a name `dnsdb.yml`.

Entries in the **Local DNS DB** file should be represented as follows:
```
---

# entry with single subdomain
1.2.3.4:
  subdomains:
    - a.b.c
  comment: "my comments for this subdomain list"

# entry with multiple subdomains per IP
11.22.33.44:
  subdomains:
    - nms.mts
    - vsd.mts
  comment: ""

# an empty subdomain section will clear all existing entries for this IP in Remote DNS DB
11.22.33.44:
  subdomains:
    -
  comment: ""

```

### API access & tokens
To be able to use Yandex API a tocken should be acquired and filled in the `CONSTANTS` section of a script. Refer to [this](https://tech.yandex.com/domain/doc/concepts/access-docpage/) section for guidance.

### Constants
A set of constants is used (because I was lazy to parse args)
```python
# CONSTANTS
DNS_DB_FNAME = 'dnsdb.yml'  # filname of DNS database
headers = {'PddToken': 'yourYandexAPIToken'}  # a token to auth api calls
DOMAIN = 'yourdomain.net'  # your domain managed by Yandex DNS
# API URLs
add_url = 'https://pddimp.yandex.ru/api2/admin/dns/add'
get_url = 'https://pddimp.yandex.ru/api2/admin/dns/list?domain=yourdomain.net'
del_url = 'https://pddimp.yandex.ru/api2/admin/dns/del'
# HTTP proxies if any
proxies = {
    'http': 'http://138.203.144.49:1080',
    'https': 'http://138.203.144.49:1080',
}
```
### Logging
Script uses pythons logging engine. Logging setup parameters decoupled into a separate file `log_setup.yml`.
Log entries can be found in `yadm.log` file.

### HTTP proxies
To keep it simple a list of HTTP proxies have been hardcoded:
```python
proxies = {
    'http': 'http://138.203.144.49:1080',
    'https': 'https://138.203.144.49:1080',
}
```
If you have a direct connection just comment out the `http` & `https` key/value pairs so proxies
dict will appear empty, or simply declare an empty `proxies = {}` dict in the `CONSTANTS` section.

## Requirements
yadm relies on these non-core modules which you can install with pip:
- `pyyaml`
- `requests`
Both Python2.7 and Python3.4+ are supported.

## Limitations and issues
### Valid hostnames
No domain name validation checks are implemented in the code so far. So it will be possible to populate local DNS DB with sudomain name like `te !st`
but this will fail upon subdomain push procedure.

## License
Licensed under MIT license.
